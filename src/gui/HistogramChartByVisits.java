package gui;
import java.awt.Color;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.SortedMap;

import javax.swing.JFrame;

import math.Histogram;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import processing.DataProcessor;
import domain.Flow;
import domain.TimeRange;

public class HistogramChartByVisits extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private static DecimalFormat format = new DecimalFormat ( "####0.#" );
	
	public HistogramChartByVisits ( String title, Histogram histogram ) {
	     super(title);
	     CategoryDataset dataset = createDataset ( histogram );
	     JFreeChart chart = createChart ( dataset );
	     ChartPanel chartPanel = new ChartPanel(chart);
	     chartPanel.setFillZoomRectangle(true);
	     chartPanel.setMouseWheelEnabled(true);
	     chartPanel.setPreferredSize(new Dimension(600, 370));
	     setContentPane(chartPanel);
	}
	
	public static CategoryDataset createDataset ( SortedMap<TimeRange,Flow> timedFlow ) throws Exception {
		return createDataset ( DataProcessor.makeHistogram(timedFlow,1,10) );
	}
	
	public static CategoryDataset createDataset ( Histogram histogram ) {
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		String ratioSeries = "Ratio";
		
		for ( int k=1; k< histogram.getGrid().length-3; k++ ) {
			double left = histogram.getGrid()[k];
			double right = histogram.getGrid()[k+1];
			String range = format.format ( left ) + " - " + format.format ( right );
			double value = histogram.getValues()[k];
//			double valueToShow = value / ( right - left );
			double valueToShow = value;
			
			dataset.addValue ( valueToShow, ratioSeries, range );
		}
		
        return dataset;
		
    }
	
	public static JFreeChart createChart(CategoryDataset dataset) {
	// create the chart...
		JFreeChart chart = ChartFactory.createBarChart(
				"Ratio Distribution",       // chart title
				"Range",               // domain axis label
				"Value",                  // range axis label
				dataset,                  // data
				PlotOrientation.VERTICAL, // orientation
				true,                     // include legend
				true,                     // tooltips?
				false                     // URLs?
		);
		
		chart.setBackgroundPaint(Color.white);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions (
		        CategoryLabelPositions.createUpRotationLabelPositions(
	            Math.PI / 3.0) );
		
		
		return chart;
		
	}
}

