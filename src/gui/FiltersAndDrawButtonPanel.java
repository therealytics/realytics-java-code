package gui;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class FiltersAndDrawButtonPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public FiltersAndDrawButtonPanel ( FilterMenusPanel filterMenusPanel, ButtonDraw buttonDraw ) {
		super();
		this.setLayout ( new BoxLayout(this,BoxLayout.X_AXIS) ); 
		this.add ( filterMenusPanel );
		this.add ( buttonDraw );
	}

	
}
