package gui;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import config.Parameters;

public class TimeParametersPanel extends JPanel {

    private JTextField startTextField; 
    private JTextField endTextField; 
    private JTextField exposureTextField;
    private JTextField timeStepTextField;
    private JTextField magnitudeThresholdTextField;


    public TimeParametersPanel ( String startTimeStr, String endTimeStr, long exposureTime, long timeStep, int magnitudeThreshold ) {
		super();
		
		this.startTextField = new JTextField ( startTimeStr );
		this.endTextField = new JTextField ( endTimeStr );	    
		this.exposureTextField = new JTextField ( Long.toString ( exposureTime ) );
		this.timeStepTextField = new JTextField ( Long.toString ( timeStep ) );
		this.magnitudeThresholdTextField = new JTextField ( Integer.toString ( magnitudeThreshold ) );
		
	    startTextField.setSize ( Parameters.INPUT_WIDTH, Parameters.INPUT_HEIGHT );
	    endTextField.setSize ( Parameters.INPUT_WIDTH, Parameters.INPUT_HEIGHT );
	    exposureTextField.setSize ( Parameters.INPUT_WIDTH, Parameters.INPUT_HEIGHT );
	    timeStepTextField.setSize ( Parameters.INPUT_WIDTH, Parameters.INPUT_HEIGHT );
	    magnitudeThresholdTextField.setSize ( Parameters.INPUT_WIDTH, Parameters.INPUT_HEIGHT );
	    
		JLabel labelStart = new JLabel ( " Start: " );
	    JLabel labelEnd = new JLabel ( " End: " );
	    JLabel labelExposure = new JLabel ( " Exposure (ms): " );
	    JLabel labelTimeStep = new JLabel ( " Time Step: " );
	    JLabel labelMagnitudeThreshold = new JLabel ( " Magnitude: " );
	    labelStart.setSize ( Parameters.LABEL_WIDTH, Parameters.LABEL_HEIGHT );
	    labelEnd.setSize ( Parameters.LABEL_WIDTH, Parameters.LABEL_HEIGHT );
	    labelExposure.setSize ( Parameters.LABEL_WIDTH, Parameters.LABEL_HEIGHT );
	    labelTimeStep.setSize ( Parameters.LABEL_WIDTH, Parameters.LABEL_HEIGHT );
	    labelMagnitudeThreshold.setSize ( Parameters.LABEL_WIDTH, Parameters.LABEL_HEIGHT );
	    
	    this.add ( labelStart );
	    this.add ( startTextField );
	    this.add ( labelEnd );
	    this.add ( endTextField );
	    this.add ( labelExposure );
	    this.add ( exposureTextField );
	    this.add ( labelTimeStep );
	    this.add ( timeStepTextField );
	    this.add ( labelMagnitudeThreshold );
	    this.add ( magnitudeThresholdTextField );
	    
	    this.setBorder ( new LineBorder(Color.BLACK) );
	}


	public JTextField getStartTextField() {
		return startTextField;
	}

	public JTextField getEndTextField() {
		return endTextField;
	}

	public JTextField getExposureTextField() {
		return exposureTextField;
	}

	public JTextField getTimeStepTextField() {
		return timeStepTextField;
	}

	public JTextField getMagnitudeThresholdTextField() {
		return magnitudeThresholdTextField;
	}
    
}
