package gui;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class FilterMenusPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public FilterMenusPanel ( List<JPanel> listOfMenuPanels ) {
		super();
		this.setLayout ( new BoxLayout(this,BoxLayout.Y_AXIS) ); 
		for ( JPanel panel : listOfMenuPanels ) {
			this.add ( panel );
		}
	}

	
}
