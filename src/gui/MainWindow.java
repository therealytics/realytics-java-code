package gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import app.Realytics;
import config.Parameters;
import dao.Dao;
import domain.Flow;
import domain.TimeRange;

public class MainWindow extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private static MainWindow theInstance = null;
	
	private TimeParametersPanel timeParametersPanel = null;
	private FilterMenusPanel filterMenusPanel = null;
	private FiltersAndDrawButtonPanel filtersAndDrawButtonPanel = null;
	private ButtonDraw buttonDraw = null;
	private ChartPanel chartPanel = null;
	private MainMenuPanel mainMenuPanel = null;
	private WorkPanel workPanel = null;
	
	private MainWindow(){}
	
	private MainWindow ( SortedMap<TimeRange,Flow> timedFlow, String startTimeStr, String endTimeStr, 
						long exposureTime, long timeStep, int magnitudeThreshold ) throws Exception {
		
		UIManager.setLookAndFeel ( UIManager.getCrossPlatformLookAndFeelClassName() );
		setSize ( Parameters.WINDOW_WIDTH, Parameters.WINDOW_HEIGHT );
		
	    Container container = getContentPane(); // ���������� ������� ����
	    container.setLayout ( new BoxLayout ( container, BoxLayout.X_AXIS ) ); 
	    
	    mainMenuPanel = new MainMenuPanel();	    
	    container.add ( mainMenuPanel );

	    timeParametersPanel = new TimeParametersPanel ( startTimeStr, endTimeStr, exposureTime, timeStep, magnitudeThreshold );
	    List<JPanel> listOfMenuPanels = new ArrayList<JPanel>();
	    listOfMenuPanels.add ( timeParametersPanel );
	    filterMenusPanel = new FilterMenusPanel ( listOfMenuPanels );
	    buttonDraw = new ButtonDraw ( "Draw!" );
	    filtersAndDrawButtonPanel = new FiltersAndDrawButtonPanel ( filterMenusPanel, buttonDraw );

		chartPanel = new ChartPanel ( TimeSeriesChart.createChart ( TimeSeriesChart.createDataset(timedFlow) ) );

		workPanel = new WorkPanel ( filtersAndDrawButtonPanel, chartPanel );
	    container.add ( workPanel );
	    
	    this.addWindowListener ( 
	    	new WindowAdapter() {
	            public void windowClosing(WindowEvent e) {
	                System.exit(0);
	            }
	        }
	    );
	}

	public static MainWindow getInstance() throws Exception {
		if ( theInstance == null ) {
			Dao dao = new Dao();
			Instant startTime = Instant.parse ( Realytics.DEFAULT_START_TIME_STRING );
			Instant endTime = Instant.parse ( Realytics.DEFAULT_END_TIME_STRING );
			SortedMap<TimeRange,Flow> timedFlow = dao.readTimedFlow ( startTime, endTime, Realytics.DURATION_THRESHOLD, Realytics.TIME_STEP, Realytics.MAGNITUDE_THRESHOLD );
			theInstance = new MainWindow ( timedFlow, Realytics.DEFAULT_START_TIME_STRING, Realytics.DEFAULT_END_TIME_STRING, 
					Realytics.DURATION_THRESHOLD, Realytics.TIME_STEP, Realytics.MAGNITUDE_THRESHOLD );
		}
		return theInstance;
	}
	
	@Override
	public void actionPerformed ( ActionEvent event ) {
/*		
		try {
			String command = event.getActionCommand();
			if ( textArea.getText().startsWith("a") ) {
				textArea.setText ( "It Works!" );
			}
			
			String startStr = startTextField.getText();
			String endStr = endTextField.getText();
			String exposureStr = exposureTextField.getText();
			String timeStepStr = timeStepTextField.getText();
			String magnitudeThresholdStr = magnitudeThresholdTextField.getText();
			Instant startTime = Instant.parse ( startStr );
			Instant endTime = Instant.parse ( endStr );
			long exposureTime = Long.parseLong ( exposureStr );
			long timeStep = Long.parseLong ( timeStepStr );
			int magnitudeThreshold = Integer.parseInt ( magnitudeThresholdStr );
			
			SortedMap<TimeRange,Flow> timedFlow = Realytics.DAO.readTimedFlow ( startTime, endTime, exposureTime, timeStep, magnitudeThreshold );
			updateGraphComponent ( timedFlow );
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/		
	}

	private void updateGraphComponent ( SortedMap<TimeRange,Flow> timedFlow ) throws Exception {

		JFreeChart chart = TimeSeriesChart.createChart ( TimeSeriesChart.createDataset(timedFlow) );
//		JFreeChart chart = HistogramChartByVisits.createChart ( HistogramChartByVisits.createDataset ( timedFlow ) );
		
		chartPanel.setChart ( chart );
	}
}
