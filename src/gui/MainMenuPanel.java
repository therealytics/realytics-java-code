package gui;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class MainMenuPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JButton curveButton = new ButtonTimeLine ( "������" );
	private JButton histogramButton = new ButtonHistogram ( "����������" );
	
	public MainMenuPanel() {
		super();
		this.setLayout ( new BoxLayout ( this, BoxLayout.Y_AXIS ) );
		this.add ( curveButton );
		this.add ( histogramButton );
		
		Border border = new LineBorder ( Color.BLACK );
		this.setBorder ( border );
	}

	
}
