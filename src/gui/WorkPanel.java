package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.jfree.chart.ChartPanel;

public class WorkPanel extends JPanel implements ActionListener {

	public static final int NO_CHART_STATE = -1;
	public static final int TIME_SERIES_CHART_STATE = 0;
	public static final int THISTORGAM_CHART_STATE = 1;
	
	private int chartState = NO_CHART_STATE;
	
	public WorkPanel ( FiltersAndDrawButtonPanel filtersAndDrawButtonPanel, ChartPanel chartPanel ) {
		super();
		this.setLayout ( new BoxLayout(this,BoxLayout.Y_AXIS) ); 
		this.add ( filtersAndDrawButtonPanel );
		this.add ( chartPanel );
	}

	@Override
	public void actionPerformed ( ActionEvent event ) {
		// TODO Auto-generated method stub
		
	}

	
}
