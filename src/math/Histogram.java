package math;

public class Histogram {

	private double[] grid; // All points including the outer left and right ones.
	private double[] values; // The distribution
	
	public Histogram(double[] grid, double[] values) throws Exception {
		super();
		this.grid = grid;
		this.values = values;
		
		validate();
	}

	public void addValue ( double value ) {
		
		if ( value>=grid[0] && value<=grid[grid.length-1] ) {
			int k=0;
			while (  k<values.length && value>grid[k+1] ) {
				k++;
			}
			values[k] += 1.0;
		}
	}
	
	public void normalize() {
		double sum = 0.0;
		for ( int k=0; k<values.length; k++ ) {
			sum += values[k];
		}
		if ( sum > 0.0 ) {
			for ( int k=0; k<values.length; k++ ) {
				values[k] = values[k] / sum;
			}			
		}
	}
	
	private void validate() throws Exception {
		if ( grid == null ) {
			throw new Exception ( "No grid." );
		}
		else if ( values == null ) {
			throw new Exception ( "No array of values." );
		}
		else if ( grid.length != values.length+1 ) {
			throw new Exception ( "Wrong dimensions of the arrays grid and values." );
		}

	}
	
	public double[] getGrid() {
		return grid;
	}
	public void setGrid(double[] grid) {
		this.grid = grid;
	}

	public double[] getValues() {
		return values;
	}
	public void setValues(double[] values) {
		this.values = values;
	}
	
}
