package app;

import gui.MainWindow;
import gui.TimeSeriesChart;

import java.time.Instant;
import java.util.SortedMap;

import javax.swing.JPanel;

import dao.Dao;
import domain.Flow;
import domain.TimeRange;

public class Realytics {
	
	public static final long DURATION_THRESHOLD = 600000;
	public static final long TIME_STEP = 3600;  // seconds
	public static final int MAGNITUDE_THRESHOLD = -60;  // decibells
	public static final String DEFAULT_START_TIME_STRING = "2015-10-24T00:00:00.0000Z";
	public static final String DEFAULT_END_TIME_STRING = "2015-10-24T23:00:00.0000Z";

	public static Dao DAO = null;
	
	public static void main(String[] args) throws Exception {
/*		
		System.out.println ( "Result: " );
		for ( Map.Entry<TimeRange,Flow> entry : timedFlow.entrySet() ) {
			System.out.println ( entry.getKey().toString() + "   " + entry.getValue().getSetOfVisitors().size() + " visitors" );
		}
*/		
		
		MainWindow window = MainWindow.getInstance();
		window.setVisible(true);
	}

}
