package config;

public class Parameters {

	public static final int WINDOW_WIDTH = 1200;
	public static final int WINDOW_HEIGHT = 900;
	
	public static final int LABEL_WIDTH = 110;
	public static final int LABEL_HEIGHT = 40;
	public static final int INPUT_WIDTH = 110;
	public static final int INPUT_HEIGHT = 40;
}
