package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import app.Realytics;
import domain.Flow;
import domain.Signal;
import domain.TimeRange;
import domain.Visitor;
import domain.VisitorLocalBehavior;

public class Dao {

	private static final long TIME_STEP_IN_MILLIS = 1000 * Realytics.TIME_STEP;  // milliseconds
	private static final String PATTERN = "YYYY-MM-dd HH:mm:mm.SSS";
	private static final String PATTERN_SQL = "YYYY-MM-DD HH24:MI:SS.MS";
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_INSTANT;
	
	private Connection con = null;
	
	public Dao() {
        
        String url = "jdbc:postgresql://92.63.100.202:5432/monik?sslfactory=org.postgresql.ssl.NonValidatingFactory";
        String user = "vlob";
        String password = "Eaci2oog";
        
        Properties props = new Properties();
        props.setProperty ( "user", user );
        props.setProperty ( "password", password );
        props.setProperty ( "ssl", "true" );
        
            try {
				con = DriverManager.getConnection(url, props);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
	}
	
	
	public void testConnection() {
		try {
			PreparedStatement pst = con.prepareStatement("SELECT * FROM probes limit 5");
			ResultSet rs = pst.executeQuery();
			System.out.println ( "Success!" );
			while ( rs.next() ) {
				String macStr = rs.getString("mac");
				System.out.println ( macStr );
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public SortedMap<TimeRange,Flow> readTimedFlow ( Instant startTime, Instant endTime, long exposureTime, long timeStep, int signalMagnitudeThreshold ) {
		
		long timeStepInMillis = 1000 * timeStep;
		
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Calendar calendar = Calendar.getInstance(tz);
		
		SortedMap<TimeRange,Flow> mapOfTimeRangeToFlow = new TreeMap<TimeRange,Flow>();

		try {			
			Map<String,Visitor> mapOfMacToVisitor = new HashMap<String,Visitor>();
			List<TimeRange> listOfTimeRanges = new ArrayList<TimeRange>();
			
			Instant startOfRange = startTime;
			do {
				Instant endOfRange = startOfRange.plusMillis ( timeStepInMillis );
				TimeRange theRange = new TimeRange ( startOfRange, endOfRange );
				listOfTimeRanges.add ( theRange );				
				startOfRange = endOfRange;
			} while ( startOfRange.isBefore(endTime) );
			
			String startTimeStr = FORMATTER.format ( startTime );
			String endTimeStr = FORMATTER.format ( endTime );
			String whereClause = "where timestamp>=to_timestamp('" + startTimeStr + "','" + PATTERN_SQL 
					+ "') and timestamp<to_timestamp('" + endTimeStr + "','" + PATTERN_SQL + "')";
			String orderClause = " order by timestamp";
			String sqlString = "SELECT source, timestamp, mac, signal FROM probes " + whereClause + orderClause;
			System.out.println ( sqlString );
			PreparedStatement pst = con.prepareStatement ( sqlString );
			ResultSet rs = pst.executeQuery();
			
			Iterator<TimeRange> timeRangeIterator = listOfTimeRanges.iterator();
			TimeRange currentRange = timeRangeIterator.next();
			
			Map<Visitor,VisitorLocalBehavior> visitorsBehavior = new HashMap<Visitor,VisitorLocalBehavior>();

			while ( rs.next() ) {
				String macStr = rs.getString("mac");
				String sourceStr = rs.getString("source");
				Timestamp timestamp = rs.getTimestamp ( "timestamp", calendar );
				Instant theInstant = timestamp.toInstant();
				int signalLevel = rs.getInt ( "signal" );
				
				if ( signalLevel >= signalMagnitudeThreshold ) {
					Signal signal = new Signal ( theInstant, signalLevel, macStr, sourceStr );
	
					while ( !currentRange.isInstantInRange(theInstant) && timeRangeIterator.hasNext() ) {
						mapOfTimeRangeToFlow.put ( currentRange, makeFlow(currentRange,visitorsBehavior,exposureTime) );
						currentRange = timeRangeIterator.next();
						visitorsBehavior = new HashMap<Visitor,VisitorLocalBehavior>();
					}
					
					Visitor visitor = mapOfMacToVisitor.get ( macStr );
					if ( visitor == null ) {
						visitor = new Visitor ( macStr );
						mapOfMacToVisitor.put ( macStr, visitor );				}
					
					VisitorLocalBehavior behavior = visitorsBehavior.get ( visitor );
					if ( behavior == null ) {
						behavior = new VisitorLocalBehavior();
						visitorsBehavior.put ( visitor, behavior );
					}
					behavior.addSignal ( signal );			
				}
			}
			
			System.out.println ( "Success!" );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mapOfTimeRangeToFlow;
	}
	
	private Flow makeFlow ( TimeRange theRange, Map<Visitor,VisitorLocalBehavior> visitorsBehavior, long exposure ) {
		
		Set<Visitor> setOfVisitors = new HashSet<Visitor>();
		for ( Map.Entry<Visitor,VisitorLocalBehavior> entry : visitorsBehavior.entrySet() ) {
			VisitorLocalBehavior behavior = entry.getValue();
			if ( behavior.findDurationInMilliseconds() >= exposure ) {
				setOfVisitors.add ( entry.getKey() );
			}
		}
		
		return new Flow ( theRange, setOfVisitors );
	}
}
