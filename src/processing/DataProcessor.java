package processing;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

import math.Histogram;
import domain.Flow;
import domain.TimeRange;
import domain.Visitor;

public class DataProcessor {

	public static Histogram makeHistogram ( SortedMap<TimeRange,Flow> timedFlow, int minValue, int maxValue ) throws Exception {
		
		double[] grid = new double[maxValue-minValue+1];
		double[] values = new double[grid.length-1];
		
		for ( int k=0; k<grid.length; k++ ) {
			grid[k] = k + 1;
		}
		
		Histogram histogram = new Histogram ( grid, values );
		
		Map<Visitor,Integer> mapOfVisitorToCount = new HashMap<Visitor,Integer>();
		
		for ( Map.Entry<TimeRange,Flow> entry : timedFlow.entrySet() ) {
			Flow flow = entry.getValue();
			for ( Visitor v : flow.getSetOfVisitors() ) {
				Integer count = mapOfVisitorToCount.get ( v );
				if ( count == null ) {
					mapOfVisitorToCount.put ( v, new Integer(1) );
				}
				else {
					int newCount = count + 1;
					mapOfVisitorToCount.put ( v, new Integer(newCount) );
				}
			}
		}
		
		for ( Map.Entry<Visitor,Integer> entry : mapOfVisitorToCount.entrySet() ) {
			
			histogram.addValue ( (double)entry.getValue() );
		}
		
		return histogram;
	}
}
