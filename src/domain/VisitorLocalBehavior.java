package domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class VisitorLocalBehavior {

	private final List<Signal> listOfVisitorSignals;

	public VisitorLocalBehavior(List<Signal> listOfVisitorSignals) {
		super();
		this.listOfVisitorSignals = listOfVisitorSignals;
	}
	

	public VisitorLocalBehavior() {
		super();
		listOfVisitorSignals = new ArrayList<Signal>();
	}


	public List<Signal> getListOfVisitorSignals() {
		return listOfVisitorSignals;
	}
	
	public int findMaxLevelOfSignal() {
		return 0;
	}
	
	public long findDurationInMilliseconds() {
		if ( listOfVisitorSignals==null || listOfVisitorSignals.size()<2 ) {
			return 0L;
		}
		Signal firstSignal = listOfVisitorSignals.get ( 0 );
		Signal lastSignal = listOfVisitorSignals.get ( listOfVisitorSignals.size()-1 );
		Instant firstTime = firstSignal.getTime();
		Instant lastTime = lastSignal.getTime();
		
		return ( lastTime.toEpochMilli() - firstTime.toEpochMilli() );
	}
	
	public void addSignal ( Signal signal ) {
		listOfVisitorSignals.add ( signal );
	}
}
