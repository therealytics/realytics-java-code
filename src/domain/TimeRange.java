package domain;

import java.time.Instant;

public class TimeRange implements Comparable<TimeRange>{

	private final Instant startTime;  // inclusive
	private final Instant endTime;  // exclusive
	
	public TimeRange(Instant startTime, Instant endTime) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public Instant getStartTime() {
		return startTime;
	}
	public Instant getEndTime() {
		return endTime;
	}

	public boolean isInstantInRange ( Instant instant ) {
		return ( instant!=null && !instant.isBefore(startTime) && instant.isBefore(endTime) );
	}

	@Override
	public int compareTo ( TimeRange other ) {
		return ( this.startTime.compareTo(other.startTime) );
	}

	@Override
	public String toString() {
		return ( startTime.toString() + " - " + endTime.toString() );
	}
	
	
}
