package domain;

import java.util.List;

public class Visit {

	private final long visitId;
	private final List<Signal> listOfSignals;
	private final Visitor visitor;
	
	public Visit(long visitId, List<Signal> listOfSignals, Visitor visitor) {
		super();
		this.visitId = visitId;
		this.listOfSignals = listOfSignals;
		this.visitor = visitor;
	}
	
	public long getVisitId() {
		return visitId;
	}
	public List<Signal> getListOfSignals() {
		return listOfSignals;
	}
	public Visitor getVisitor() {
		return visitor;
	}
	
}
