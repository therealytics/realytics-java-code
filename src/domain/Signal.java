package domain;

import java.time.Instant;

public class Signal {

	private final Instant time;
	private final int logMag;
	private final String mac;
	private final String routerId;
	
	public Signal ( Instant time, int logMag, String mac, String routerId ) {
		super();
		this.time = time;
		this.logMag = logMag;
		this.mac = mac;
		this.routerId = routerId;
	}

	public Instant getTime() {
		return time;
	}

	public int getLogMag() {
		return logMag;
	}

	public String getMac() {
		return mac;
	}

	public String getRouterId() {
		return routerId;
	}
	

}
