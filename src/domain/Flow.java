package domain;

import java.util.Set;

public class Flow {

	private final TimeRange timeRange;
	private final Set<Visitor> setOfVisitors;
	
	public Flow(TimeRange timeRange, Set<Visitor> setOfVisitors) {
		super();
		this.timeRange = timeRange;
		this.setOfVisitors = setOfVisitors;
	}

	public TimeRange getTimeRange() {
		return timeRange;
	}

	public Set<Visitor> getSetOfVisitors() {
		return setOfVisitors;
	}
	
}
